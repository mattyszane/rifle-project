from queries.pool import pool
from models.accounts import AccountIn, Account


class DuplicateAccountError(ValueError):
    pass


class AccountQueries:
    def get_user_account(self, username: str) -> Account:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT id, username, hashed_password
                    FROM accounts
                    WHERE username = %s
                    """,
                    [
                        username
                    ]
                )
                result = cur.fetchone()
                if result is None:
                    return None
                account = {}
                for i, col in enumerate(cur.description):
                    account[col.name] = result[i]
                return Account(
                    id=result[0],
                    username=result[1],
                    hashed_password=result[2]
                )

    def create_account(
            self, account: AccountIn, hashed_password: str) -> Account:
        existing_account = self.get_user_account(account.username)
        if existing_account is not None:
            raise DuplicateAccountError()
        with pool.connection() as conn:
            with conn.cursor() as cur:
                params = [
                    account.username,
                    hashed_password,
                ]
                cur.execute(
                    """
                    INSERT INTO accounts (username, hashed_password)
                    VALUES (%s, %s)
                    RETURNING id, username
                    """,
                    params,
                )

                record = None
                row = cur.fetchone()
                if row is not None:
                    record = {}
                    for i, col in enumerate(cur.description):
                        record[col.name] = row[i]
                id = row[0]
                return Account(
                    id=id,
                    username=account.username,
                    hashed_password=hashed_password
                )

    def get_all_accounts(self):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT id, username, hashed_password
                    FROM accounts
                    """
                )
                results = []
                for row in cur.fetchall():
                    account = {}
                    for i, col in enumerate(cur.description):
                        account[col.name] = row[i]
                    results.append(account)
                return results
