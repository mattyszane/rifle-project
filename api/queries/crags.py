from models.crags import CragIn
from queries.pool import pool


class CragsQueries():
    def create_crag(self, crag: CragIn, added_by: str):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                params = [
                    crag.name,
                    crag.description,
                    added_by
                ]
                cur.execute(
                    """
                    INSERT INTO crags (
                        name, description, added_by)
                    VALUES (%s, %s, %s)
                    RETURNING id, name, description, added_by
                    """,
                    params,
                )

                record = None
                row = cur.fetchone()
                if row is not None:
                    record = {}
                    for i, col in enumerate(cur.description):
                        record[col.name] = row[i]
                return record


    def get_all_crags(self):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT id, name, description, added_by
                    FROM crags
                    """
                )
                results = []
                for row in cur.fetchall():
                    crag = {}
                    for i, col in enumerate(cur.description):
                        crag[col.name] = row[i]
                    results.append(crag)
                return results
