from fastapi import APIRouter, Depends
from models.reviews import ReviewIn, ReviewList, ReviewOut
from authenticator import authenticator
from queries.reviews import ReviewsQueries


router = APIRouter()


@router.post("/api/reviews", response_model=ReviewOut)
def create_review(
    review_in: ReviewIn,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: ReviewsQueries = Depends(),
):
    return queries.create_review(
        review_in,
        user_id=account_data["id"],
        username=account_data["username"]
        )

@router.get("/api/reviews/mine", response_model=ReviewList)
def user_review_list(
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: ReviewsQueries = Depends(),
):
    reviews = queries.get_user_reviews(user_id=account_data["id"])
    return {"reviews": reviews}


@router.get("/api/reviews/{climb}", response_model=ReviewList)
def climb_review_list(
    climb: str,
    queries: ReviewsQueries = Depends()
):
    reviews = queries.get_climb_reviews(climb=climb)
    return {"reviews": reviews}
