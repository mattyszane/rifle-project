from pydantic import BaseModel
from typing import List


class Climb(BaseModel):
    name: str

class CragOut(BaseModel):
    name: str
    description: str
    added_by: str

class CragIn(BaseModel):
    name: str
    description: str


class Crag(BaseModel):
    name: str
    description: str
    added_by: str

class CragList(BaseModel):
    crags: list[CragOut]
