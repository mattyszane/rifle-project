import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./App.css";
import MainPage from "./MainPage";

function App() {
  return (
    <BrowserRouter>
    <div className="full-height" id="page-container">
    <Routes>
      <Route path="/" element={<MainPage />} />

    </Routes>
    <div className="push"></div>
    </div>

    </BrowserRouter>
  )
}

export default App;
