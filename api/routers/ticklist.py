from authenticator import authenticator
from queries.ticklist import TickQueries
from models.ticklist import(
    TickIn,
    TickOut,
    TickList
)
from fastapi import(
    Depends,
    APIRouter,
    HTTPException,
    status
)


router = APIRouter()


@router.post("/api/ticks", response_model=TickOut)
def create_tick(
    tick_in: TickIn,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: TickQueries = Depends(),
):
    try:
        return queries.create_tick(tick_in, username=account_data["username"])
    except HTTPException:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Park already added to favorites.",
        )
