from pydantic import BaseModel

class ClimbOut(BaseModel):
    id: int
    name: str
    grade: str
    description: str
    crag: str

class ClimbIn(BaseModel):
    name: str
    grade: str
    description: str
    crag: str

class ClimbList(BaseModel):
    climbs: list[ClimbOut]

class Climb(BaseModel):
    id: int
    name: str
    grade: str
    description: str
    crag: str
