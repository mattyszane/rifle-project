from models.climbs import ClimbIn
from queries.pool import pool


class ClimbsQueries():
    def create_climb(self, climb: ClimbIn):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                params = [
                    climb.name,
                    climb.grade,
                    climb.description,
                    climb.crag
                ]
                cur.execute(
                    """
                    INSERT INTO climbs (
                        name, grade, description, crag)
                    VALUES(%s, %s, %s, %s)
                    RETURNING id, name, grade, description, crag
                    """,
                    params,
                )

                record = None
                row = cur.fetchone()
                if row is not None:
                    record = {}
                    for i, col in enumerate(cur.description):
                        record[col.name] = row[i]
                return record


    def get_all_climbs(self):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT id, name, grade, description, crag
                    FROM climbs
                    """
                )
                results = []
                for row in cur.fetchall():
                    climb = {}
                    for i, col in enumerate(cur.description):
                        climb[col.name] = row[i]
                    results.append(climb)
                return results

    def get_climbs_by_crag(self, crag: str):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT id, name, grade, description, crag
                    FROM climbs
                    WHERE crag = %s
                    """,
                    [
                        crag
                    ]
                )
                results = []
                for row in cur.fetchall():
                    climb = {}
                    for i, col in enumerate(cur.description):
                        climb[col.name] = row[i]
                    results.append(climb)
                return results
