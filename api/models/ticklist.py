from pydantic import BaseModel

class TickIn(BaseModel):
    climb: str


class TickOut(TickIn):
    id: int
    username: str


class Ticks(BaseModel):
    id: int
    climb: str
    username: str


class TickList(BaseModel):
    ticks: list[TickOut]
