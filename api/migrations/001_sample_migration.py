steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE accounts (
            id SERIAL PRIMARY KEY NOT NULL,
            username VARCHAR(1000) NOT NULL UNIQUE,
            hashed_password VARCHAR(1000) NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE accounts;
        """
    ],

    [
        """
        CREATE TABLE crags (
        id SERIAL PRIMARY KEY NOT NULL,
        name VARCHAR(1000) NOT NULL UNIQUE,
        description VARCHAR(5000) NOT NULL,
        added_by VARCHAR(1000) NOT NULL REFERENCES accounts("username")
        );
        """,
        """
        DROP TABLE crags;
        """
    ],

    [
        """
        CREATE TABLE climbs (
        id SERIAL PRIMARY KEY NOT NULL,
        name VARCHAR(1000) NOT NULL,
        grade VARCHAR(1000) NOT NULL,
        description VARCHAR(5000) NOT NULL,
        crag VARCHAR(1000) NOT NULL REFERENCES crags("name")
        );
        """,
        """
        DROP TABLE climbs;
        """
    ],

    [
        """
        CREATE TABLE reviews(
        id SERIAL PRIMARY KEY NOT NULL,
        climb VARCHAR(1000) NOT NULL,
        user_id INTEGER NOT NULL REFERENCES accounts("id"),
        username VARCHAR(1000) NOT NULL REFERENCES accounts("username"),
        comment VARCHAR(1000) NOT NULL,
        rating INTEGER NOT NULL
        );
        """,
        """
        DROP TABLE reviews;
        """
    ],

    [
        """
        CREATE TABLE ticklist(
        id SERIAL PRIMARY KEY NOT NULL,
        username VARCHAR(1000) NOT NULL REFERENCES accounts("username"),
        climb VARCHAR(50) NOT NULL
        );
        """,
        """
        DROP TABLE ticklist;
        """
    ]

]
