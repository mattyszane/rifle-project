from pydantic import BaseModel


class ReviewOut(BaseModel):
    id: int
    comment: str
    climb: str
    user_id: int
    username: str
    rating: int


class ReviewIn(BaseModel):
    comment: str
    rating: int
    climb: str


class ReviewList(BaseModel):
    reviews: list[ReviewOut]
