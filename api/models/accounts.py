from pydantic import BaseModel
from jwtdown_fastapi.authentication import Token


class AccountIn(BaseModel):
    username: str
    password: str


class Account(BaseModel):
    id: int
    username: str
    hashed_password: str


class AccountOut(BaseModel):
    id: int
    username: str
    hashed_password: str


class AccountList(BaseModel):
    accounts: list[AccountOut]


class AccountForm(BaseModel):
    username: str
    password: str


class AccountToken(Token):
    account: AccountOut


class HttpError(BaseModel):
    detail: str
