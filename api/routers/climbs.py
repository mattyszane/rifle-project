from fastapi import APIRouter, Depends
from models.climbs import ClimbIn, ClimbList, ClimbOut
from authenticator import authenticator
from queries.climbs import ClimbsQueries


router = APIRouter()


@router.post("/api/climbs", response_model=ClimbOut)
def create_climb(
    climb_in: ClimbIn,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: ClimbsQueries = Depends(),
):
    return queries.create_climb(
        climb_in
    )



@router.get("/api/climbs", response_model=ClimbList)
def climb_list(
    queries: ClimbsQueries = Depends(),
):
    climbs = queries.get_all_climbs()
    return {"climbs": climbs}


@router.get("/api/climbs/{crag}", response_model=ClimbList)
def get_climbs_by_crag(
    crag: str,
    queries: ClimbsQueries = Depends(),
):
    climbs = queries.get_climbs_by_crag(crag=crag)
    return {"climbs": climbs}
