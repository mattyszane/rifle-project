from models.ticklist import TickIn, TickOut, Ticks
from queries.pool import pool



class TickQueries():

    def create_tick(self, ticks: TickIn, username: str):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    INSERT INTO ticklist
                    (username, climb)
                    VALUES
                    (%s, %s)
                    RETURNING id;
                    """,
                    [
                        username,
                        ticks.climb
                    ]
                )
                id = cur.fetchone()[0]
                data = ticks.dict()
                return TickOut(id=id, username=username, **data)


    def get_user_ticks(self, username: str):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT id, username, climb
                    FROM ticklist
                    WHERE username = %s
                    """,
                    [
                        username
                    ]
                )
                rows = cur.fetchall()
                ticks = []
                for row in rows:
                    id, username, climb = row
                    tick = TickOut(
                        id=id, username=username, climb=climb)
                    ticks.append(tick)
                return ticks
