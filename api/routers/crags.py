from fastapi import APIRouter, Depends
from models.crags import CragIn, CragList, CragOut
from authenticator import authenticator
from queries.crags import CragsQueries

router = APIRouter()


@router.post("/api/crags", response_model=CragOut)
def create_crag(
    crag_in: CragIn,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: CragsQueries = Depends(),
):
    return queries.create_crag(
        crag_in,
        added_by=account_data["username"]
    )

@router.get("/api/crags", response_model=CragList)
def crag_list(
    queries: CragsQueries = Depends(),
):
    crags = queries.get_all_crags()
    return {"crags": crags}


